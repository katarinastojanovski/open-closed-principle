﻿using Moq;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using Xunit;

namespace KS.Examples.OpenClosedPrinciple.Serialization.FieldTypeRetrievers
{
    public class FieldTypePriorityRetrieverTests
    {
        [Fact]
        public void GetFieldTypeShouldReturnHigherPriorityTypeWhenTwoTypesCanBeDeserialized()
        {
            // Arrange
            var type1 = typeof(string);
            var type2 = typeof(decimal);
            var priorityOneDeserializationFieldTypeMock = new Mock<IDeserializationFieldType>();
            var priorityTwoDeserializationFieldTypeMock = new Mock<IDeserializationFieldType>();
            priorityOneDeserializationFieldTypeMock.Setup(c => c.Priority).Returns(1);
            priorityTwoDeserializationFieldTypeMock.Setup(c => c.Priority).Returns(2);
            priorityOneDeserializationFieldTypeMock.Setup(c => c.Type).Returns(type1);
            priorityTwoDeserializationFieldTypeMock.Setup(c => c.Type).Returns(type2);
            var jsonObject = new JObject();
            priorityOneDeserializationFieldTypeMock.Setup(c => c.CanBeDeserialized(jsonObject)).Returns(true);
            priorityTwoDeserializationFieldTypeMock.Setup(c => c.CanBeDeserialized(jsonObject)).Returns(true);

            var deserializationFieldTypes = new List<IDeserializationFieldType>()
            {
                priorityOneDeserializationFieldTypeMock.Object,
                priorityTwoDeserializationFieldTypeMock.Object,
            };
            var fieldTypeRetriever = new FieldTypePriorityRetriever(deserializationFieldTypes);
            var expectedType = typeof(decimal);

            // Act
            var result = fieldTypeRetriever.GetFieldType(jsonObject);

            // Assert
            Assert.Equal(expectedType, result);
        }

        [Fact]
        public void GetFieldTypeShouldReturnTypeWhenTypeCanBeDeserialized()
        {
            // Arrange
            var type1 = typeof(string);
            var type2 = typeof(decimal);
            var canBeDeseriliazedFieldTypeMock = new Mock<IDeserializationFieldType>();
            var cannotBeDeseriliazedFieldTypeMock = new Mock<IDeserializationFieldType>();
            var jsonObject = new JObject();
            canBeDeseriliazedFieldTypeMock.Setup(c => c.CanBeDeserialized(jsonObject)).Returns(true);
            cannotBeDeseriliazedFieldTypeMock.Setup(c => c.CanBeDeserialized(jsonObject)).Returns(false);
            canBeDeseriliazedFieldTypeMock.Setup(c => c.Type).Returns(type1);
            cannotBeDeseriliazedFieldTypeMock.Setup(c => c.Type).Returns(type2);

            var deserializationFieldTypes = new List<IDeserializationFieldType>()
            {
                canBeDeseriliazedFieldTypeMock.Object,
                cannotBeDeseriliazedFieldTypeMock.Object,
            };
            var fieldTypeRetriever = new FieldTypePriorityRetriever(deserializationFieldTypes);
            var expectedType = typeof(string);

            // Act
            var result = fieldTypeRetriever.GetFieldType(jsonObject);

            // Assert
            Assert.Equal(expectedType, result);
        }

        [Fact]
        public void GetFieldTypeShouldThrowExceptionWhenTypeCannotBeDeserialized()
        {
            // Arrange
            var deserializationFieldTypeMock = new Mock<IDeserializationFieldType>();
            var jsonObject = new JObject();
            deserializationFieldTypeMock.Setup(c => c.CanBeDeserialized(jsonObject)).Returns(false);

            var deserializationFieldTypes = new List<IDeserializationFieldType>()
            {
                deserializationFieldTypeMock.Object,
            };
            var fieldTypeRetriever = new FieldTypePriorityRetriever(deserializationFieldTypes);

            // Act
            Action act = () => fieldTypeRetriever.GetFieldType(jsonObject);

            // Assert
            Assert.Throws<NotSupportedException>(act);
        }

        [Fact]
        public void GetFieldTypeShouldThrowExceptionWhenNoDeserializationFieldTypesExist()
        {
            // Arrange
            var jsonObject = new JObject();
            var deserializationFieldTypes = new List<IDeserializationFieldType>();
            var fieldTypeRetriever = new FieldTypePriorityRetriever(deserializationFieldTypes);

            // Act
            Action act = () => fieldTypeRetriever.GetFieldType(jsonObject);

            // Assert
            Assert.Throws<NotSupportedException>(act);
        }
    }
}