﻿using Xunit;

namespace KS.Examples.OpenClosedPrinciple.Serialization.FieldTypeRetrievers
{
    public class StringDeserializationFieldTypeTests
    {
        [Theory]
        [InlineData("stringfield.json", true)]
        [InlineData("timespanfield.json", true)]
        [InlineData("booleanfield.json", false)]
        [InlineData("novalue.json", false)]
        public void CanBeDeserialized(string filename, bool expected)
        {
            // Arrange
            var fieldType = new StringDeserializationFieldType();
            JsonFileLoader jsonFileLoader = new JsonFileLoader("JsonFiles");
            var jsonObject = jsonFileLoader.GetJsonObjectFromTextFile(filename);

            // Act
            var result = fieldType.CanBeDeserialized(jsonObject);

            // Assert
            Assert.Equal(expected, result);
        }
    }
}