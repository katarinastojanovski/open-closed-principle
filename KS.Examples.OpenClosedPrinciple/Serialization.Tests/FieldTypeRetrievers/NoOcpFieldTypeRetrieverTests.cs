﻿using KS.Examples.OpenClosedPrinciple.Domain;
using System;
using Xunit;

namespace KS.Examples.OpenClosedPrinciple.Serialization.FieldTypeRetrievers
{
    public class NoOcpFieldTypeRetrieverTests
    {
        [Theory]
        [InlineData(typeof(BooleanField), "booleanfield.json")]
        [InlineData(typeof(StringField), "stringfield.json")]
        [InlineData(typeof(TimeSpanField), "timespanfield.json")]
        [InlineData(typeof(DateTimeField), "datetimefield.json")]
        public void GetFieldTypeShouldReturnTypeWhenTypeCanBeDeserialized(Type fieldType, string filename)
        {
            // Arrange
            JsonFileLoader jsonFileLoader = new JsonFileLoader("JsonFiles");
            var jsonObject = jsonFileLoader.GetJsonObjectFromTextFile(filename);
            var fieldTypeRetriever = new NoOcpFieldTypeRetriever();

            // Act
            var result = fieldTypeRetriever.GetFieldType(jsonObject);

            // Assert
            Assert.Equal(fieldType, result);
        }

        [Fact]
        public void GetFieldTypeShouldReturnBooleanFieldWhenTypeCanBeDeserialized()
        {
            // Arrange
            JsonFileLoader jsonFileLoader = new JsonFileLoader("JsonFiles");
            var jsonObject = jsonFileLoader.GetJsonObjectFromTextFile("booleanfield.json");
            var fieldTypeRetriever = new NoOcpFieldTypeRetriever();

            // Act
            var result = fieldTypeRetriever.GetFieldType(jsonObject);

            // Assert
            Assert.Equal(typeof(BooleanField), result);
        }
    }
}