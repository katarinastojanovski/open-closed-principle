using Xunit;

namespace KS.Examples.OpenClosedPrinciple.Serialization.FieldTypeRetrievers
{
    public class BooleanDeserializationFieldTypeTests
    {
        [Theory]
        [InlineData("booleanfield.json", true)]
        [InlineData("stringfield.json", false)]
        [InlineData("novalue.json", false)]
        public void CanBeDeserialized(string filename, bool expected)
        {
            // Arrange
            var fieldType = new BooleanDeserializationFieldType();
            JsonFileLoader jsonFileLoader = new JsonFileLoader("JsonFiles");
            var jsonObject = jsonFileLoader.GetJsonObjectFromTextFile(filename);

            // Act
            var result = fieldType.CanBeDeserialized(jsonObject);

            // Assert
            Assert.Equal(expected, result);
        }
    }
}