﻿using Newtonsoft.Json.Linq;
using System.IO;

namespace KS.Examples.OpenClosedPrinciple.Serialization
{
    internal class JsonFileLoader
    {
        private string jsonFilesFolderPath;

        public JsonFileLoader(string jsonFilesFolderPath)
        {
            this.jsonFilesFolderPath = jsonFilesFolderPath;
        }

        public JObject GetJsonObjectFromTextFile(string filename)
        {
            var fileContent = this.GetJsonStringFromTextFile(filename);
            var jsonObject = JObject.Parse(fileContent);
            return jsonObject;
        }

        public string GetJsonStringFromTextFile(string filename)
        {
            return File.ReadAllText($"{this.jsonFilesFolderPath}/{filename}");
        }
    }
}
