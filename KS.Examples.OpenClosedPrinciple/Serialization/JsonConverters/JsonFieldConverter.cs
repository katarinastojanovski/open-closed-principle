﻿using KS.Examples.OpenClosedPrinciple.Domain;
using KS.Examples.OpenClosedPrinciple.Serialization.FieldTypeRetrievers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace KS.Examples.OpenClosedPrinciple.Serialization.JsonConverters
{
    internal class JsonFieldConverter : JsonConverter
    {
        private readonly IFieldTypeRetriever fieldTypeRetriever;

        public JsonFieldConverter(IFieldTypeRetriever fieldConverterRetriever)
        {
            this.fieldTypeRetriever = fieldConverterRetriever;
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Field);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var jsonObject = JObject.Load(reader);
            var type = this.fieldTypeRetriever.GetFieldType(jsonObject);
            return jsonObject.ToObject(type, serializer);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}