﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace KS.Examples.OpenClosedPrinciple.Serialization
{
    internal class SerializerService : ISerializerService
    {
        private readonly IContractResolver customContractResolver;

        public SerializerService(IContractResolver customContractResolver)
        {
            this.customContractResolver = customContractResolver;
        }

        public T Deserialize<T>(string input)
        {
            var settings = new JsonSerializerSettings
            {
                ContractResolver = this.customContractResolver,
            };
            return JsonConvert.DeserializeObject<T>(input, settings);
        }

        public string Serialize<T>(T input)
        {
            var settings = new JsonSerializerSettings()
            {
                NullValueHandling = NullValueHandling.Ignore,
            };
            return JsonConvert.SerializeObject(input, settings);
        }
    }
}
