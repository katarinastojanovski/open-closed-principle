﻿namespace KS.Examples.OpenClosedPrinciple.Serialization
{
    public interface IDoer
    {
        bool CanDoSomething(params object[] myParameters);

        object DoSomething(params object[] myParameters);
    }
}
