﻿using KS.Examples.OpenClosedPrinciple.Serialization.FieldTypeRetrievers;
using KS.Examples.OpenClosedPrinciple.Serialization.JsonConverters;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace KS.Examples.OpenClosedPrinciple.Serialization
{
    public static class DependencyInjectionManualSetup
    {
        public static void UseSerializationWithOrder(this IServiceCollection services)
        {
            services.AddSingleton<IDeserializationFieldType, BooleanDeserializationFieldType>();
            services.AddSingleton<IDeserializationFieldType, TimeSpanDeserializationFieldType>();
            services.AddSingleton<IDeserializationFieldType, StringDeserializationFieldType>();
            services.AddSingleton<IDeserializationFieldType, DateTimeDeserializationFieldType>();
            services.AddSingleton<IFieldTypeRetriever, FieldTypeRetriever>();

            services.AddSingleton<JsonConverter, JsonFieldConverter>();
            services.AddSingleton<IContractResolver, CustomContractResolver>();
            services.AddSingleton<ISerializerService, SerializerService>();
        }
    }
}
