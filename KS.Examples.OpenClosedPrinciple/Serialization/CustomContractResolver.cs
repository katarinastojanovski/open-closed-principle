﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KS.Examples.OpenClosedPrinciple.Serialization
{
    internal class CustomContractResolver : CamelCasePropertyNamesContractResolver
    {
        private readonly IEnumerable<JsonConverter> registeredConverters;

        public CustomContractResolver(IEnumerable<JsonConverter> registeredConverters)
        {
            this.registeredConverters = registeredConverters;
        }

        protected override JsonContract CreateContract(Type objectType)
        {
            JsonContract contract = base.CreateContract(objectType);
            JsonConverter jsonConverter = this.registeredConverters.Where(c => c.CanConvert(objectType)).FirstOrDefault();
            if (jsonConverter != null)
            {
                contract.Converter = jsonConverter;
            }

            return contract;
        }
    }
}
