﻿using Newtonsoft.Json.Linq;
using System;

namespace KS.Examples.OpenClosedPrinciple.Serialization.FieldTypeRetrievers
{
    internal abstract class DeserializationFieldTypeBase : IDeserializationFieldType
    {
        public virtual int Priority { get; } = 1;

        public abstract Type Type { get; }

        public abstract bool CanBeDeserialized(JObject jsonObject);

        protected JToken GetValueToken(JObject jsonObject)
        {
            return jsonObject["value"];
        }
    }
}