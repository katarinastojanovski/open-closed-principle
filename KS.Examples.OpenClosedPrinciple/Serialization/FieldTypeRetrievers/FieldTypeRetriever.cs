﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace KS.Examples.OpenClosedPrinciple.Serialization.FieldTypeRetrievers
{
    internal class FieldTypeRetriever : IFieldTypeRetriever
    {
        private readonly IEnumerable<IDeserializationFieldType> deserializationFieldTypes;

        public FieldTypeRetriever(IEnumerable<IDeserializationFieldType> deserializationFieldTypes)
        {
            this.deserializationFieldTypes = deserializationFieldTypes;
        }

        public Type GetFieldType(JObject jsonObject)
        {
            foreach (var deserializationFieldType in this.deserializationFieldTypes)
            {
                if (deserializationFieldType.CanBeDeserialized(jsonObject))
                {
                    return deserializationFieldType.Type;
                }
            }

            throw new NotSupportedException();
        }
    }
}