﻿using Newtonsoft.Json.Linq;
using System;

namespace KS.Examples.OpenClosedPrinciple.Serialization.FieldTypeRetrievers
{
    internal interface IDeserializationFieldType
    {
        int Priority { get; }

        Type Type { get; }

        bool CanBeDeserialized(JObject jsonObject);
    }
}