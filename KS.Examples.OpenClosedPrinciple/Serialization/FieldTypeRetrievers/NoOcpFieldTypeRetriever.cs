﻿using KS.Examples.OpenClosedPrinciple.Domain;
using Newtonsoft.Json.Linq;
using System;

namespace KS.Examples.OpenClosedPrinciple.Serialization.FieldTypeRetrievers
{
    internal class NoOcpFieldTypeRetriever : IFieldTypeRetriever
    {
        public Type GetFieldType(JObject jsonObject)
        {
            var jsonToken = jsonObject["value"];

            if (jsonToken == null)
            {
                throw new NotSupportedException();
            }

            if (jsonToken.Type == JTokenType.Boolean)
            {
                return typeof(BooleanField);
            }

            if (jsonToken.Type == JTokenType.Date)
            {
                return typeof(DateTimeField);
            }

            // This check must be done before StringField.
            if (jsonToken.Type == JTokenType.String && TimeSpan.TryParse(jsonToken.Value<string>(), out _))
            {
                return typeof(TimeSpanField);
            }

            if (jsonToken.Type == JTokenType.String)
            {
                return typeof(StringField);
            }

            throw new NotSupportedException();
        }
    }
}