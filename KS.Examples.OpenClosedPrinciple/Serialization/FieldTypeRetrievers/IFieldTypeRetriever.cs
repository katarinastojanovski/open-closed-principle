﻿using Newtonsoft.Json.Linq;
using System;

namespace KS.Examples.OpenClosedPrinciple.Serialization.FieldTypeRetrievers
{
    internal interface IFieldTypeRetriever
    {
        Type GetFieldType(JObject jsonObject);
    }
}