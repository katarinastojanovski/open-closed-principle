﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KS.Examples.OpenClosedPrinciple.Serialization.FieldTypeRetrievers
{
    internal class FieldTypePriorityRetriever : IFieldTypeRetriever
    {
        private readonly IEnumerable<IDeserializationFieldType> deserializationFieldTypes;

        public FieldTypePriorityRetriever(IEnumerable<IDeserializationFieldType> deserializationFieldTypes)
        {
            this.deserializationFieldTypes = deserializationFieldTypes;
        }

        public Type GetFieldType(JObject jsonObject)
        {
            foreach (var deserializationFieldType in this.deserializationFieldTypes.OrderByDescending(c => c.Priority))
            {
                if (deserializationFieldType.CanBeDeserialized(jsonObject))
                {
                    return deserializationFieldType.Type;
                }
            }

            throw new NotSupportedException();
        }
    }
}