﻿using KS.Examples.OpenClosedPrinciple.Domain;
using Newtonsoft.Json.Linq;
using System;

namespace KS.Examples.OpenClosedPrinciple.Serialization.FieldTypeRetrievers
{
    internal class StringDeserializationFieldType : DeserializationFieldTypeBase
    {
        public override Type Type { get; } = typeof(StringField);

        public override bool CanBeDeserialized(JObject jsonObject)
        {
            var jsonToken = this.GetValueToken(jsonObject);
            if (jsonToken == null)
            {
                return false;
            }

            return jsonToken.Type == JTokenType.String;
        }
    }
}