﻿using KS.Examples.OpenClosedPrinciple.Domain;
using Newtonsoft.Json.Linq;
using System;

namespace KS.Examples.OpenClosedPrinciple.Serialization.FieldTypeRetrievers
{
    internal class TimeSpanDeserializationFieldType : DeserializationFieldTypeBase
    {
        public override int Priority { get; } = 2;

        public override Type Type { get; } = typeof(TimeSpanField);

        public override bool CanBeDeserialized(JObject jsonObject)
        {
            var jsonToken = this.GetValueToken(jsonObject);
            if (jsonToken == null || jsonToken.Type != JTokenType.String)
            {
                return false;
            }

            return TimeSpan.TryParse(jsonToken.Value<string>(), out _);
        }
    }
}