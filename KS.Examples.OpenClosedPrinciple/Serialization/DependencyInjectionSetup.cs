﻿using KS.Examples.OpenClosedPrinciple.Serialization.FieldTypeRetrievers;
using KS.Examples.OpenClosedPrinciple.Serialization.JsonConverters;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace KS.Examples.OpenClosedPrinciple.Serialization
{
    public static class DependencyInjectionSetup
    {
        public static void UseSerialization(this IServiceCollection services)
        {
            if (services is null)
            {
                throw new ArgumentNullException(nameof(services));
            }

            var deserializationFieldTypes = GetTypes<IDeserializationFieldType>();
            foreach (var deserializationFieldType in deserializationFieldTypes)
            {
                services.Add(new ServiceDescriptor(typeof(IDeserializationFieldType), deserializationFieldType, ServiceLifetime.Singleton));
            }

            services.AddSingleton<IFieldTypeRetriever, FieldTypePriorityRetriever>();

            services.AddSingleton<JsonConverter, JsonFieldConverter>();
            services.AddSingleton<IContractResolver, CustomContractResolver>();
            services.AddSingleton<ISerializerService, SerializerService>();
        }

        private static IEnumerable<Type> GetTypes<T>()
        {
            var types = new List<Type>();
            Assembly assembly = Assembly.GetAssembly(typeof(T));
            foreach (Type type in assembly.GetTypes())
            {
                if (type != typeof(T) && !type.IsAbstract && typeof(T).IsAssignableFrom(type))
                {
                    types.Add(type);
                }
            }

            return types;
        }
    }
}
