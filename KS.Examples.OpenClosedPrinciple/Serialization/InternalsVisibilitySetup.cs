﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("KS.Examples.OpenClosedPrinciple.Serialization.IntegrationTests")]
[assembly: InternalsVisibleTo("KS.Examples.OpenClosedPrinciple.Serialization.Tests")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]