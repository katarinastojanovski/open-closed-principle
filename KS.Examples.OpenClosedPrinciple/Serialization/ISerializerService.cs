﻿namespace KS.Examples.OpenClosedPrinciple.Serialization
{
    public interface ISerializerService
    {
        T Deserialize<T>(string input);

        string Serialize<T>(T input);
    }
}
