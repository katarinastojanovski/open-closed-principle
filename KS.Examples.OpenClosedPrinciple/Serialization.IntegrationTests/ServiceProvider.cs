﻿using Microsoft.Extensions.DependencyInjection;

namespace KS.Examples.OpenClosedPrinciple.Serialization
{
    internal class SerializationServiceProvider
    {
        public static T GetService<T>()
        {
            var services = new ServiceCollection();
            services.UseSerialization();
            var serviceProvider = services.BuildServiceProvider();
            return serviceProvider.GetService<T>();             
        }
    }
}
