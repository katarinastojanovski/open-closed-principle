using KS.Examples.OpenClosedPrinciple.Domain;
using System;
using System.Linq;
using Xunit;

namespace KS.Examples.OpenClosedPrinciple.Serialization
{
    public class SerializerTests
    {
        [Theory]
        [InlineData(typeof(BooleanField), 0)]
        [InlineData(typeof(DateTimeField), 1)]
        [InlineData(typeof(StringField), 2)]
        [InlineData(typeof(TimeSpanField), 3)]
        public void DeserializeProductFields(Type fieldType, int index)
        {
            // Arrange
            JsonFileLoader jsonFileLoader = new JsonFileLoader("JsonFiles");
            var productJson = jsonFileLoader.GetJsonStringFromTextFile("product.json");
            var serializerService = SerializationServiceProvider.GetService<ISerializerService>();

            // Act
            var product = serializerService.Deserialize<Product>(productJson);

            // Assert
            Assert.IsType(fieldType, product.Fields[index]);
        }

        [Fact]
        public void DeserializeProductShouldReturnCorrectFieldCount()
        {
            // Arrange
            JsonFileLoader jsonFileLoader = new JsonFileLoader("JsonFiles");
            var productJson = jsonFileLoader.GetJsonStringFromTextFile("product.json");
            var serializerService = SerializationServiceProvider.GetService<ISerializerService>();

            // Act
            var product = serializerService.Deserialize<Product>(productJson);

            // Assert
            Assert.Equal(4, product.Fields.Count);
        }
    }
}
