﻿using System.Collections.Generic;

namespace KS.Examples.OpenClosedPrinciple.Domain
{
    public class Product
    {
        public string Name { get; set; }

        public List<Field> Fields { get; set; }
    }
}