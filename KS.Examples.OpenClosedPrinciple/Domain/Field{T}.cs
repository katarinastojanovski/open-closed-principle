﻿namespace KS.Examples.OpenClosedPrinciple.Domain
{
    public abstract class Field<T> : Field
    {
        public T Value { get; set; }
    }
}