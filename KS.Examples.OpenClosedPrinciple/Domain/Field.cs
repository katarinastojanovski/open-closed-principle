﻿namespace KS.Examples.OpenClosedPrinciple.Domain
{
    public abstract class Field
    {
        public string Name { get; set; }
    }
}